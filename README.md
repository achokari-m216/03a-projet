# 02b Pratique MQTT-Git [+ (28pts) +]

## Objectifs

* Créer un projet IoT basé sur les connaissances accumulées durant le cours
* Réaliser un protocole de test

## Ressources 

* [Documentation Git](https://git-scm.com/book/fr/v2)
* [Documentation paho-mqtt](https://pypi.org/project/paho-mqtt)

## Consignes

* Votre `<visa>` est toujours composé des 4 premières lettres de votre prénom et des 4 premières lettres de votre nom
* Si vous êtes bloqué(e) dans vos manipulations git, demandez à l'enseignant. Les points des parties non réalisées seront déduites mais vous pourrez continuer.
* Effectuer un fork du projet actuel et cloner-le sur votre Raspberry Pi
* Un fois terminé, effectuez un commit du projet, un push, puis un merge request

## Installation

```bash
git config --global user.name "<Prénom> <Nom>"
git config --global user.email "<prenom>.<nom>@edu.vs.ch"
pip install paho-mqtt
```

## Introduction

Dans le cours M216, vous avez pu découvrir l’intégration d’un terminal (Raspberry Pi Zero W) à une Plateforme IoE (MQTT) permettant l’échange d’informations via Wi-Fi. Vous avez pu découvrir la communication avec différents capteurs (boutons, potentiomètres, LEDs, avertisseur sonore, écrans LCD, …) et l’envoi/réception de messages au travers du protocole MQTT.

L’objectif de cette évaluation est de mettre ensemble ces différentes connaissances afin de créer un projet IoT reproduisant un exemple pratique d’utilisation d’IoE dans le domaine de votre choix (domotique, industrie, santé, agriculture).

Voici ce qui est attendu :
1. Imaginer une situation dans laquelle l’IoE pourrait résoudre des problématiques.
2. Décrire la problématique dans ce document.
3. Décrire les entrées/sorties correspondantes au GrovePi 
    * Minimum 2 capteurs, écran LCD et 1 sortie
4. Décrire les topics MQTT utilisés pour communiquer avec le projet IoE
    * Minimum 4, dont 1 retain et 1 non retain
5. Réaliser le script python (ne doit pas nécessairement être intégralement terminé).
    * Au moins deux envois MQTT
    * Au moins deux réceptions MQTT
6. Compléter le protocole de tests (au moins 10 critères)


## Instructions

* Insérez votre code source sur ce projet