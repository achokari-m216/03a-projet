#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#----------------------------------------------------------------------------
#           EPTM - Ecole professionnelle technique et des métiers
#
# Nom du fichier source              : mqtt_led_pot_lum.py
#
# Auteur (Nom, Prénom)               : Achoumi Karim
# Classe                             : EM-IN DEV 2A
# Module                             : M216
# Date de création                   : 25.03.2024
#
# Description succincte du programme :
#    Gestion d'une serre connectée à l'aide de l'IoE
#----------------------------------------------------------------------------

import random
import time

from grovepi import *
from grove_rgb_lcd import *

from paho.mqtt import client as mqtt_client


broker = 'mqtt-eptm.jcloud.ik-server.com'
port = 11521

visa_send = 'achokari'

visa_receive = 'achokari'

topic_pot_send = visa_send + "/pot"
topic_pot_receive = visa_receive + "/pot"

topic_luminosite_send = visa_send + "/luminosite"
topic_luminosite_receive = visa_receive + "/luminosite"

max_qos = 2

max_analog_read = 1023
max_luminosite = 100 # %
min_luminosite = 0 # %

max_pot_humidite = 100 # %
min_pot_humidite = 0 # %

# generate client ID with pub prefix randomly
client_id = f'python-mqtt-{random.randint(0, 100)}'
username = ''
password = ''

connected = False

luminosite = None

pinPot = 0
pinLuminosite = 1
pinLed = 3

pinMode(pinPot, "INPUT")
pinMode(pinLuminosite, "INPUT")
pinMode(pinLed, "OUTPUT")

def connect_mqtt() -> mqtt_client:
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)

    client = mqtt_client.Client(client_id)
    client.username_pw_set(username, password)
    client.on_connect = on_connect
    client.connect(broker, port)
    return client

def subscribe(client: mqtt_client):
    def on_message(client, userdata, msg):
        if msg.topic == topic_luminosite_receive :
            resultat = msg.payload.decode()
            if int(resultat) == 0:
                digitalWrite(pinLed,0)
            else:
                digitalWrite(pinLed,1)
        elif msg.topic == topic_pot_receive :
            resultat = msg.payload.decode()
            if int(resultat) == 0:
                digitalWrite(pinLed,0)
            else:
                digitalWrite(pinLed,1)
        elif msg.topic == topic_pot_send :
            # TODO
            pass

    client.subscribe(topic_luminosite_receive, max_qos)
    client.subscribe(topic_pot_receive, max_qos)
    client.subscribe(topic_pot_send, max_qos)
    client.subscribe(topic_luminosite_send, max_qos)
    client.on_message = on_message
    
def readPin(pin, min, max, rnd = 0):
    return round(analogRead(pin) / max_analog_read * (max - min) + min, rnd)
    
def formatTemp(val):
    return str(val).rjust(3)

def run():
    client = connect_mqtt()
    subscribe(client)
    client.loop_start()
    old_lum = -1
    old_pot = -1
    refresh = True
    while True:
        pot = int(readPin(pinPot, min_pot_humidite, max_pot_humidite))
        lum = int(readPin(pinLuminosite, min_luminosite, max_luminosite))
        if(old_lum != lum):
            refresh = True
            old_lum = lum
            client.publish(topic_luminosite_send,lum,0,False)
            print(f"Send `{lum}` to `{topic_luminosite_send}`")
            

        if(old_pot != pot):
            refresh = True
            old_pot = pot
            client.publish(topic_pot_send,pot,0,False)
            print(f"Send `{pot}` to `{topic_pot_send}`")
            


        if(refresh):
            refresh = False
            lumStr = str(int(lum != 0))
            potStr = formatTemp(pot)
            #lumStr = formatTemp(luminosite) + " C"
            #setText_norefresh("Btn:" + butStr +"    Pot:"+ potStr + "\nTemp:" + tempStr)
            print("\rPot: "+ potStr + "   |    Luminosité: " + lumStr + "     ", end="", flush=True)

        time.sleep(0.1)

if __name__ == '__main__':
    run()

